

import 'package:flutter/material.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  var Var = 0 ;
 @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(top: 30 , left: 10),
          child: Text("ميدان اسفينكس,المعادي,القاهره,مصر"),
        ),
        titleTextStyle: TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontWeight: FontWeight.bold
        ),
        backgroundColor: Color.fromARGB(255, 230, 230, 222),
        elevation: 0.01,
        leading: Padding(
          padding: const EdgeInsets.only(top:10, left: 15 ),
          child: Text("تغيير",
          style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
             ),
           ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(top: 7 ,),
            child: Text("المنزل",
            style: TextStyle(
              color: Colors.black,
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
            ),
          ),
          Icon(Icons.location_on , size: 50, color: Colors.yellow,),
           ],
      ),
      backgroundColor: Color.fromARGB(255, 96, 96, 91),
     body: SingleChildScrollView(
       child: Column(
        children: [
         Container(
          margin: EdgeInsets.all(10),
          
         color: Colors.yellow,
          width: double.infinity,
          height: 130,
           child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: MaterialButton(
                  color: Color.fromARGB(255, 9, 57, 96),
                  onPressed: (){},
                child: Text("تسوق الأن",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
                ),
                ),
                
                ),
              ),
            ),
            Expanded(
              child:
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Image.network("https://th.bing.com/th/id/OIP.SxS3PGNzSCzfLVj3wa4lKgHaFW?pid=ImgDet&rs=1"),
               ))  
            ],
           ),
         ),
        SizedBox(height: 10,),
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 10 , left: 10),
                child: Image.network("https://th.bing.com/th/id/OIP.bA7bRPzJi_YzxvOratpZ3AHaEK?pid=ImgDet&rs=1"),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(right: 10 , left: 10),
                child: Image.network("https://th.bing.com/th/id/OIP.hPQ2kZNIeueJWbimacy8sQHaEK?w=297&h=180&c=7&r=0&o=5&dpr=1.25&pid=1.7"),
              ),
            )
          ],
     
        ),
        SizedBox(height: 10,),
        Row(
          children: [
           Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.air , size: 40,color: Colors.yellow,),
               ),
               Text("تكييف",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
             Expanded(
            child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.electrical_services , size: 40,color: Colors.yellow,),
               ),
               Text("كهرباء",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
             Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.plumbing , size: 40,color: Colors.yellow,),
               ),
               Text("السباكة",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
     
     
     
     
     
     
           
     
          ],
        ),
        SizedBox(height: 10,),
        Row(
          children: [
           Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.format_paint , size: 40,color: Colors.yellow,),
               ),
               Text("نقاش",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
             Expanded(
            child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.person , size: 40,color: Colors.yellow,),
               ),
               Text("مقاول",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
             Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.carpenter , size: 40,color: Colors.yellow,),
               ),
               Text("النجارة",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
     
     
     
     
     
     
           
     
          ],
        ),
        SizedBox(height: 10,),
        Row(
          children: [
           Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.electrical_services , size: 40,color: Colors.yellow,),
               ),
               Text("كهرباء",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
             Expanded(
            child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.wash, size: 40,color: Colors.yellow,),
               ),
               Text("غسالات",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
             Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(
                 padding: const EdgeInsets.all(10.0),
                 child: Icon(Icons.cleaning_services , size: 40,color: Colors.yellow,),
               ),
               Text("تنظيف",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
              ],
            ),
            )
            ),
     
     
     
     
     
     
           
     
          ],
        ),
         ],
                 ),
     ),
     
      bottomNavigationBar: BottomNavigationBar(
        type:  BottomNavigationBarType.fixed,
            currentIndex: Var,
           onTap:(index){
            setState(() {
              Var = index;
            });
           },
         items: [
           BottomNavigationBarItem(
             label: "الطلبات",
             icon: Icon(Icons.local_offer , size: 40,), 
           ),
           
           BottomNavigationBarItem(
             label: "العروض", 
             icon:Icon(Icons.bookmark_border , size: 40,)
           ),
          
            BottomNavigationBarItem(
             label: "الرئيسية",
             icon: Icon(Icons.home , size: 40,)
           ),
            BottomNavigationBarItem(
             label: "الرئيسية",
             icon: Icon(Icons.home , size: 40,)
           ),
          
         ],
         backgroundColor: Colors.yellow,
         selectedFontSize: 20,
         iconSize: 30,
         
         
         ),

    );
  }
}