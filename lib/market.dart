

// ignore_for_file: unused_import

import 'package:flutter/material.dart';
//import 'package:geolocator/geolocator.dart';

class MarketScreen extends StatelessWidget {
  const MarketScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
      leading: Icon(Icons.shopping_cart,size: 40,color: Colors.amber,),
      actions: [Icon(Icons.arrow_forward, size: 40,color: Colors.amber,)],
      backgroundColor: Colors.cyan,
      centerTitle: true,
      title: Text("السوق",
      style: TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.w600,
        color: Colors.amber
      ),
      ),
     ),
     body: SingleChildScrollView(
       child: Column(
        children: [
          Row(
            children: [
               Expanded(
                  child: Container(
                    color: Colors.cyan,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Text("الترتيب", 
                          style: TextStyle(
                            color: Colors.amber,
                            fontSize: 25 ,
                            fontWeight: FontWeight.bold
                          ),
                          ),
                        ),
                        Icon(Icons.unfold_more , size: 40,color: Colors.amber,),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 2,),
                Expanded(
                  child: Container(
                    color: Colors.cyan,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Text("التصفية", 
                          style: TextStyle(
                            color: Colors.amber,
                            fontSize: 25 ,
                            fontWeight: FontWeight.bold
                          ),
                          ),
                        ),
                         Icon(Icons.filter_alt_outlined , size: 40,color: Colors.amber,)
                      ],
                    ),
                  ),
                ),
               
            ],
          ),
          SizedBox(height: 10,),
        Padding(
       padding: const EdgeInsets.all(8.0),
       child: Row(
              children: [
              Expanded(
                 child: Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                     Icon(Icons.favorite_border , size: 25,),
                 //    Image.network("https://i.ytimg.com/vi/RWXgCQNQIUo/hqdefault.jpg"),
                     Text("تكييف تورنيدو اسبليت بارد ديجيتال , تبريد فائق السرعة",
                     style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Text("5.000 جنيه",
                     style: TextStyle(
                      color: Colors.amber,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          maxRadius: 20,
                          child: Icon(
                            Icons.shopping_bag,size: 30,color: Colors.white,),
                        ),
                        SizedBox(width: 65,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                             color: Color.fromARGB(255, 200, 198, 198),
                          ),
                            child: Row(
                            children: [
                              Text("أسطي",
                              style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                              ),
                              SizedBox(width: 10,),
                              Icon(Icons.collections,size: 30, color: Colors.black,)
                            ],
                          ),
                        ),
                      ],
                     )
     
               
                    ],
                  ),
                 ),
               ),
               SizedBox(width: 15,),
                Expanded(
                 child: Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                     Icon(Icons.favorite_border , size: 25,),
                     Image.network("https://i.ytimg.com/vi/RWXgCQNQIUo/hqdefault.jpg"),
                     Text("تكييف تورنيدو اسبليت بارد ديجيتال , تبريد فائق السرعة",
                     style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Text("5.000 جنيه",
                     style: TextStyle(
                      color: Colors.amber,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          maxRadius: 20,
                          child: Icon(
                            Icons.shopping_bag,size: 30,color: Colors.white,),
                        ),
                        SizedBox(width: 65,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                             color: Color.fromARGB(255, 200, 198, 198),
                          ),
                            child: Row(
                            children: [
                              Text("شارب",
                              style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                              ),
                              SizedBox(width: 10,),
                              Icon(Icons.collections,size: 30, color: Colors.black,)
                            ],
                          ),
                        ),
                      ],
                     )
     
               
                    ],
                  ),
                 ),
               ),
              ],
       ),
        ),
        SizedBox(height: 10,),
        Padding(
       padding: const EdgeInsets.all(8.0),
       child: Row(
              children: [
              Expanded(
                 child: Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                     Icon(Icons.favorite_border , size: 25,),
                     Image.network("https://i.ytimg.com/vi/RWXgCQNQIUo/hqdefault.jpg"),
                     Text("تكييف تورنيدو اسبليت بارد ديجيتال , تبريد فائق السرعة",
                     style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Text("5.000 جنيه",
                     style: TextStyle(
                      color: Colors.amber,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          maxRadius: 20,
                          child: Icon(
                            Icons.shopping_bag,size: 30,color: Colors.white,),
                        ),
                        SizedBox(width: 65,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                             color: Color.fromARGB(255, 200, 198, 198),
                          ),
                            child: Row(
                            children: [
                              Text("أسطي",
                              style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                              ),
                              SizedBox(width: 10,),
                              Icon(Icons.collections,size: 30, color: Colors.black,)
                            ],
                          ),
                        ),
                      ],
                     )
     
               
                    ],
                  ),
                 ),
               ),
               SizedBox(width: 15,),
                Expanded(
                 child: Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                     Icon(Icons.favorite_border , size: 25,),
                     Image.network("https://i.ytimg.com/vi/RWXgCQNQIUo/hqdefault.jpg"),
                     Text("تكييف تورنيدو اسبليت بارد ديجيتال , تبريد فائق السرعة",
                     style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Text("5.000 جنيه",
                     style: TextStyle(
                      color: Colors.amber,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          maxRadius: 20,
                          child: Icon(
                            Icons.shopping_bag,size: 30,color: Colors.white,),
                        ),
                        SizedBox(width: 65,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                             color: Color.fromARGB(255, 200, 198, 198),
                          ),
                            child: Row(
                            children: [
                              Text("شارب",
                              style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                              ),
                              SizedBox(width: 10,),
                              Icon(Icons.collections,size: 30, color: Colors.black,)
                            ],
                          ),
                        ),
                      ],
                     )
     
               
                    ],
                  ),
                 ),
               ),
              ],
       ),
        ),
        SizedBox(height: 10,),
        Padding(
       padding: const EdgeInsets.all(8.0),
       child: Row(
              children: [
              Expanded(
                 child: Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                     Icon(Icons.favorite_border , size: 25,),
                     Image.network("https://i.ytimg.com/vi/RWXgCQNQIUo/hqdefault.jpg"),
                     Text("تكييف تورنيدو اسبليت بارد ديجيتال , تبريد فائق السرعة",
                     style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Text("5.000 جنيه",
                     style: TextStyle(
                      color: Colors.amber,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          maxRadius: 20,
                          child: Icon(
                            Icons.shopping_bag,size: 30,color: Colors.white,),
                        ),
                        SizedBox(width: 65,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                             color: Color.fromARGB(255, 200, 198, 198),
                          ),
                            child: Row(
                            children: [
                              Text("أسطي",
                              style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                              ),
                              SizedBox(width: 10,),
                              Icon(Icons.collections,size: 30, color: Colors.black,)
                            ],
                          ),
                        ),
                      ],
                     )
     
               
                    ],
                  ),
                 ),
               ),
               SizedBox(width: 15,),
                Expanded(
                 child: Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                     Icon(Icons.favorite_border , size: 25,),
                     Image.network("https://i.ytimg.com/vi/RWXgCQNQIUo/hqdefault.jpg"),
                     Text("تكييف تورنيدو اسبليت بارد ديجيتال , تبريد فائق السرعة",
                     style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Text("5.000 جنيه",
                     style: TextStyle(
                      color: Colors.amber,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                     ),
                     Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          maxRadius: 20,
                          child: Icon(
                            Icons.shopping_bag,size: 30,color: Colors.white,),
                        ),
                        SizedBox(width: 65,),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                             color: Color.fromARGB(255, 200, 198, 198),
                          ),
                            child: Row(
                            children: [
                              Text("شارب",
                              style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w400
                     ),
                              ),
                              SizedBox(width: 10,),
                              Icon(Icons.collections,size: 30, color: Colors.black,)
                            ],
                          ),
                        ),
                      ],
                     )
     
               
                    ],
                  ),
                 ),
               ),
              ],
       ),
        ),
        
     
     
     
     
     
        ],
       ),
     ),

    );
    
  }
}