import 'dart:math';

import 'package:flutter/material.dart';

class TheDice extends StatefulWidget {
  const TheDice({Key? key}) : super(key: key);

  @override
  State<TheDice> createState() => _TheDiceState();
}

class _TheDiceState extends State<TheDice> {
  int right = 2;
  int left = 1;
  Roll(){
    setState(() {
      left = Random().nextInt(6)+1;
      right = Random().nextInt(6)+1;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("The Dice App"),
        actions: [
          Text("ROLL ONE DICE")
        ],
      ),
      body: Column(
        children: [
          Center(child: Text("Shack The Throw ",
          style: TextStyle(
            fontSize: 15
          ),
          )),

          Row(
            children: [
              Expanded(child: Image.asset("images/$left.png")),
               Expanded(child: Image.asset("images/$right.png")),
            ],
          )
          
        ],
      ),
    );
  }
}