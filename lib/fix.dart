// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unused_import

import 'package:flutter/material.dart';
class FixoScreen extends StatelessWidget {
  const FixoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Image.network("https://th.bing.com/th/id/OIP.qnzLKT5z-i93Zt_nvncsXQHaEK?pid=ImgDet&rs=1",
            ),
            ),
            Text("يرجي تحديد اللغه",
            style: TextStyle(
              color: Colors.white,
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
            ),
            SizedBox(height: 15,),
             Text("Please Select The Language",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
            ),
            SizedBox(height: 100,),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromARGB(255, 198, 197, 197),
              ),
              margin: EdgeInsets.only(left: 15,right: 15),
              width: double.infinity,
               child: MaterialButton(
                onPressed: (){},
              child: Text("العربية",
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              
              ),
              
              
              ),
            ),
             SizedBox(height: 20,),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromARGB(255, 198, 197, 197),
              ),
              margin: EdgeInsets.only(left: 15,right: 15),
              width: double.infinity,
               child: MaterialButton(
                onPressed: (){},
              child: Text("English",
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              
              ),
              
              
              ),
            )
          ],
        ),
      ),
    );
    
  }
}

class FixedScreen extends StatelessWidget {
  const FixedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black, 
      body: Column(
        children: [
           Padding(
              padding: const EdgeInsets.all(20),
              child: Image.network("https://th.bing.com/th/id/OIP.qnzLKT5z-i93Zt_nvncsXQHaEK?pid=ImgDet&rs=1",
            ),
            ),
            TextFormField(
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white
                  ),
                ),
                label: Text(" *رقم الجوال ",
                style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
                ),
              ),
            ),
            SizedBox(height: 25,),
             TextFormField(
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white
                  ),
                ),
                label: Text(" *كلمة المرور ",
                style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
                ),
              ),
            ),
            SizedBox(height: 30,),
             Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromARGB(255, 198, 197, 197),
              ),
              margin: EdgeInsets.only(left: 15,right: 15),
              width: double.infinity,
               child: MaterialButton(
                onPressed: (){},
              child: Text("تسجيل الدخول",
              style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              ),
             ),
            ),
            SizedBox(height: 20,),
            Text("نسيت كلمة المرور ؟",
             style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("طلب مساعدة",
             style: TextStyle(
                color: Colors.tealAccent,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
               Text(" هل ترغب في المساعدة؟",
             style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
           
            
              ],
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
             Text("تسجيل جديد",
             style: TextStyle(
                color: Colors.tealAccent,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
               Text(" لا أمتلك حساب؟",
             style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),

            ],)
            
        ],
      ),
    );
  }
}