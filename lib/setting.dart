// ignore_for_file: unnecessary_import

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Setting extends StatelessWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Settings"),
      ),
      body: Center(
        child: ListView.builder(
         itemCount: 6,
        itemBuilder: ((context, i) {
          return Column(
            children: [
              Container(
                color: Colors.black,
                child: Row(
                  children: [
                    Icon(Icons.person,size: 30,color: Colors.white,),
                    SizedBox(width: 180,),
                    Text("Person" , 
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                    SizedBox(width: 150,),
                    Icon(Icons.arrow_forward_ios , size: 20,color: Colors.white,),
                    
                  ],
                  
                ),
              ),
              SizedBox(height: 15,)
            ],
          );
          
        }),
     

        ),
      ),
    );
  }
}