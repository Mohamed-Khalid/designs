// ignore_for_file: prefer_const_constructors_in_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class ResgisterScreen extends StatefulWidget {
  ResgisterScreen({Key? key}) : super(key: key);

  @override
  State<ResgisterScreen> createState() => _ResgisterScreenState();
}

class _ResgisterScreenState extends State<ResgisterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 3, 56, 99),
       appBar: AppBar(
        backgroundColor:Color.fromARGB(255, 3, 56, 99), 
        // ignore: prefer_const_literals_to_create_immutables
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 40),
            child: Icon(Icons.arrow_forward , size: 30,),
          )
        ],
       ),
       body: SingleChildScrollView(
         child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            Center(
              child: Text("التسجيل",
              style: TextStyle(
                color: Colors.white,
                fontSize: 40,
                fontWeight: FontWeight.bold,
              ),),
            ),
            SizedBox(height: 15,),
            Text("برجاء ادخال تفاصيل حسابك البنكي",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                Container(
                     decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromARGB(255, 6, 134, 238),
                     ),
                    margin: EdgeInsets.only(right: 20, left: 20, top: 20),
                    width: 150,
                     child: MaterialButton(
                      onPressed: (){},
                     child: Text("شركات",
                     style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                     ),
                     ),
                     ),
                   ),
                 Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color.fromARGB(255, 6, 134, 238),
                     ),
                    margin: EdgeInsets.only(right: 20, left: 20, top: 20),
                    width: 150,
                     child: MaterialButton(
                      onPressed: (){},
                     child: Text("أفراد",
                     style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white,
                     ),
                     ),
                     ),
                   ),
              ],
            ),
            SizedBox(height: 10,),
            TextFormField(
              decoration: InputDecoration(
                 label: Text(" اسم صاحب الحساب ",
                style: TextStyle(
                color: Colors.white,
               fontSize: 20,
               fontWeight: FontWeight.bold,
             ),
                ),
              ),
            ),
             SizedBox(height: 10,),
            TextFormField(
              decoration: InputDecoration(
                hintText: " 2369 1456 5632 9574 ",
                hintStyle: TextStyle(
                  color: Colors.yellow,
                  fontSize: 20,
                ),
                 label:Text("رقم الحساب",                 
                style: TextStyle(
                color: Colors.white,
               fontSize: 20,
               fontWeight: FontWeight.bold,
             ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            TextFormField(
              decoration: InputDecoration(
                 label: Text(" اسم البنك ",
                style: TextStyle(
                color: Colors.white,
               fontSize: 20,
               fontWeight: FontWeight.bold,
             ),
                ),
              ),
            ),
            SizedBox(height: 10,),
            TextFormField(
              decoration: InputDecoration(
                 label: Text(" IBAN رقم حساب ",
                style: TextStyle(
                color: Colors.white,
               fontSize: 20,
               fontWeight: FontWeight.bold,
             ),
                ),
              ),
            ),
            SizedBox(height: 30,),
             Container(
                width: double.infinity,
                color: Colors.yellow,
                margin: EdgeInsets.only( top: 160 ),
                child: MaterialButton(
                  onPressed: (){},
                 child: Text("تأكيد",
                 style: TextStyle(
                  fontSize: 30,
                  color: Colors.black,
                 ),
                 ),
                 ),
              ),


          ],
         ),
       ),
    );
  }
}