import 'package:flutter/material.dart';

class TAskScreen extends StatelessWidget {
  const TAskScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
      backgroundColor: Color.fromARGB(255, 27, 7, 100),
      leading: Icon(Icons.notifications_outlined , size: 40,),
      actions: [
        Icon(Icons.menu , size: 40,)
      ],
     ),
     backgroundColor: Color.fromARGB(255, 228, 224, 224),
     body: SingleChildScrollView(
       child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  color: Colors.grey,
                  child: Center(
                    child: Text("المنتجات", 
                    style: TextStyle(
                      fontSize: 25 ,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 2,),
              Expanded(
                child: Container(
                  color: Colors.grey,
                  child: Center(
                    child: Text("الخدمات", 
                    style: TextStyle(
                      fontSize: 25 ,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Row(children: [
            Expanded(
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  color: Colors.grey,
                  child: Center(
                    child: Text("االضمان", 
                    style: TextStyle(
                      fontSize: 25 ,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  color: Colors.grey,
                  child: Center(
                    child: Text("المنتهية", 
                    style: TextStyle(
                      fontSize: 25 ,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                  ),
                ),
              ),
            Expanded(
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  color: Colors.grey,
                  child: Center(
                    child: Text("الحالية", 
                    style: TextStyle(
                      fontSize: 25 ,
                      fontWeight: FontWeight.bold
                    ),
                    ),
                  ),
                ),
              ),
            ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10 , top: 10),
              child: Text("10/01/2021 - 06:00 pm",
              style: TextStyle(
                fontSize: 20,
              ),
              ),
            ),
            Container(
              width: double.infinity,
              color: Colors.white,
              margin: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                 Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Text("رقم الطلب : 89858695 ",
                   style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                   ),
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Text("السباكه-حمام",
                   style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                   ),
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Text(
                    "70 درهم",
                   style: TextStyle(
                    color: Colors.red,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                   ),
                   ),
                 ),
                 Container(
                  width: double.infinity,
                  color: Color.fromARGB(255, 194, 190, 190),
                   child: Center(
                     child: Text("لا يوجد فني متاح",
                     style: TextStyle(
                      color: Color.fromARGB(255, 28, 3, 96),
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                     ),
                     ),
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.only(top: 15, bottom: 15),
                   child: Row(
                    children: [
                       Expanded(
                child: Container(
                    child: Center(
                      child: Text("خروج", 
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 20 ,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                ),
              ),
                Expanded(
                child: Container(
                    child: Center(
                      child: Text("طلب المساعدة", 
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 20 ,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                ),
              ),
                Expanded(
                child: Container(
                    child: Center(
                      child: Text("اعادة المحاوله", 
                      style: TextStyle(
                        color: Colors.yellow,
                        fontSize: 20 ,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                ),
              ),
                    ],
                   ),
                 ),
     
     
     
                ],
              ),
            ),
           Padding(
              padding: const EdgeInsets.only(left: 10 , top: 10),
              child: Text("10/01/2021 - 06:00 pm",
              style: TextStyle(
                fontSize: 20,
              ),
              ),
            ),
           
            Container(
              width: double.infinity,
              color: Colors.white,
              margin: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                 Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Text("رقم الطلب : 89858695 ",
                   style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                   ),
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Text("السباكه-حمام",
                   style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                   ),
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Text(
                    "70 درهم",
                   style: TextStyle(
                    color: Colors.red,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                   ),
                   ),
                 ),
                 Container(
                  width: double.infinity,
                  color: Color.fromARGB(255, 194, 190, 190),
                   child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                       Center(
                         child: Text("التفاصيل ستظهر عندما يتم تعيين فني",
                         style: TextStyle(
                          color: Color.fromARGB(255, 28, 3, 96),
                          fontSize: 23,
                          fontWeight: FontWeight.bold,
                         ),
                         ),
                         
                       ),
                        Icon(Icons.alarm,size: 30,color: Colors.blue,),
                     ],
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.only(top: 15, bottom: 15),
                   child: Row(
                    children: [
                       Expanded(
                child: Container(
                    child: Center(
                      child: Text("الغاء الطلب", 
                      style: TextStyle(
                        color: Colors.yellow,
                        fontSize: 25 ,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                ),
              ),
                Expanded(
                child: Container(
                    child: Center(
                      child: Text("طلب تعديل ", 
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 25 ,
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                ),
              ),
                    ],
                   ),
                 ),
     
     
     
                ],
              ),
            ),
        ],
     
        
       ),
     ),

    );
  }
}